//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <iostream>

//Function Dec

int intmax (int a, int b);
double fMax (double a, double b);
double fMin (double a, double b);

template <typename Type>
Type min (Type a, Type b);


int main()
{
    

    double num1 = 9.8;
    double num2 = 8.2;
    double answer;
    
    answer = fMax(num1, num2);
    
    std::cout << "Max val = " << answer << std::endl;
    
    return 0;
}

//Function Def

//Int Max
int intmax (int a, int b)
{
    std::cout << "Max for Int\n";
    
    if (a < b)
    {
        return b;
    }
    else
    {
        return a;
    }
    
};

//Floating double max
double fMax (double a, double b)
{
    std::cout << "Max for doubles" << std::endl;
    
    if (a < b)
    {
        return b;
    }
    else
    {
        return a;
    }
};

//Floating Double Min

template <typename Type>
Type min (Type a, Type b)
{
    std::cout << "Min finder" << std::endl;
    
    if (a < b)
        return a;
    else
        return b;
    
    return 0;
};
